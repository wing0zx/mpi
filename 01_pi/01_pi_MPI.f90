program main
  use mpi
  implicit none
  real(8) x, y, pi, pi0
  integer(8) n, i, im, p, q, localn
  integer total
  integer err
  integer size, rank
  integer send, recv
  double precision st, en, elapsed

  call MPI_Init(err) ! MPIプログラムの定型句
  call MPI_Comm_size(MPI_COMM_WORLD, size, err)
  call MPI_Comm_rank(MPI_COMM_WORLD, rank, err)

  call MPI_Barrier(MPI_COMM_WORLD, err) ! 時間計測、開始の足並みをそろえる
  st = MPI_Wtime()

  !write(*,*) "円周率を推定する乱数の生成数（試行回数）" !どうしよっか
  !read(*,*) im
  im = 1000000000

  localn = int(im / size) ! プロセスごとに計算数を分割
  if(rank /= (size - 1)) then
    p = rank * localn
    q = (rank + 1) * localn - 1
  else
    p = rank * localn
    q = im
  endif

  pi0 = 2.0d0 * acos(0.0d0)
  n = 0

  do i = p, q
    call random_number(x)
    call random_number(y)
      if (x ** 2 + y ** 2 <= 1.0d0) n = n + 1
  enddo
  send = n
  recv = 0

  call MPI_Reduce(send, recv, 1, MPI_INTEGER, MPI_SUM, 0, MPI_COMM_WORLD, err)

  total = recv
  pi = 4.0d0 * dble(total) / dble(im)

  call MPI_Barrier(MPI_COMM_WORLD, err) ! 時間計測、終了の足並みをそろえる
  en = MPI_Wtime() ! 時間計測
  elapsed = en - st

  if(rank == 0) then ! 1度しか出力させない
    write(*,*) "円周率を推定する乱数の生成数（試行回数）:", im
    write(*,*)"プロセス数", size
    write(*,*)"円周率 ", pi0
    write(*,*)"推定円周率 ", pi
    write(*,*)"Elapsed time =", elapsed
  endif

  call MPI_Finalize(err) ! MPIプログラムの定型句

end program main