program main
    implicit none
        integer numnodes, numthread, numproc
        character(8) elapsetime
        character(100) program, filename
        character(1) ans

        write(*,*) "name of pragram:"
        read(*,*) program
        filename = trim(program)//".sh"

        open(10, file = filename, status = "replace")

        write(10,'(a)') "#!/bin/bash"
        write(10,'(a)') "#PJM -L rscgrp=regular"

        write(*,*) "Number of nodes:"
        read(*,*) numnodes
        write(10,'(a, i0)') "#PJM -L node=", numnodes

        write(*,*) "Is this MPI pragram? : y/n"
        read(*,*) ans
        if(ans == "y") then
            write(*,*) "Number of MPI process:"
            read(*,*) numproc
            write(10,'(a, i0)') "#PJM --mpi proc=", numproc
        endif

        write(*,*) "Is this OpenMP pragram? : y/n"
        read(*,*) ans
        if(ans == "y") then
            write(*,*) "Number of thread:"
            read(*,*) numthread
            write(10,'(a, i0)') "#PJM --omp thread=", numthread
        endif

        write(*,*) "Elapse time (ex. 00:01:00):"
        read(*,*) elapsetime
        write(10,'(a, a)') "#PJM -L elapse=", elapsetime

        write(10,'(a)') "#PJM -g jh210046"
        write(10,'(a)') "#PJM -j"

        if(ans == "y") then
            write(10,'(a, a)') "mpiexec.hydra -n ${PJM_MPI_PROC} ./", trim(program)
        else
            write(10,'(a, a)') "./", trim(program)
        endif

        close(10)
end program main