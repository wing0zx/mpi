program main
    use mpi
    implicit none
    integer :: err
    integer :: size, rank

    call MPI_Init(err)
    call MPI_Comm_size(MPI_COMM_WORLD, size, err)
    call MPI_Comm_rank(MPI_COMM_WORLD, rank, err)

    write(*,*) "Hello world! rank:", rank

    call MPI_Finalize(err)

    stop
end program main